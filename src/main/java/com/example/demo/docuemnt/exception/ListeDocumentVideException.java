package com.example.demo.docuemnt.exception;

public class ListeDocumentVideException extends  RuntimeException {

    public ListeDocumentVideException(String message) {
        super(message);
    }
}
