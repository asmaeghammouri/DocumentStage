package com.example.demo.docuemnt.repository;

import com.example.demo.docuemnt.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long> {

}
