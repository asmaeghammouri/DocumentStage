package com.example.demo.docuemnt.repository;

import com.example.demo.docuemnt.model.Document;
import com.example.demo.docuemnt.model.QDocument;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
public class QDocumenttRepositoryImpl implements QDocumentRepository{
    @Autowired
    DocumentRepository documentRepository;
    @PersistenceContext
    EntityManager entityManager;
    QDocument qDocument = QDocument.document;
    JPAQueryFactory queryFactory;

    public QDocumenttRepositoryImpl(EntityManager entityManager) {
       this.queryFactory = new JPAQueryFactory(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public List<Document> getAll() {
        return queryFactory.selectFrom(qDocument).fetch();
    }

    @Override
    public List<Document> getDocumentsParMotCle(String motCle) {
        return queryFactory.selectFrom(qDocument).where(qDocument.titre.contains(motCle)).fetch();
    }

    @Override
    public Optional<Document> findById(Long id) {
        Document result = queryFactory.selectFrom(qDocument).where(qDocument.id.eq(id)).fetchOne();
        return Optional.ofNullable(result);
    }

    @Override
    public Document save(Document documentExpected) {
        return documentRepository.save(documentExpected);
    }

}
