package com.example.demo.docuemnt.repository;

import com.example.demo.docuemnt.model.Document;

import java.util.List;
import java.util.Optional;

public interface QDocumentRepository {

    List<Document> getAll();

    List<Document> getDocumentsParMotCle(String motCle);

    Optional<Document> findById(Long id);

    Document save(Document documentExpected);
}
