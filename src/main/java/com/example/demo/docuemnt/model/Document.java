package com.example.demo.docuemnt.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Document {

    @Id
    @SequenceGenerator(
            name ="document_sequence",
            sequenceName = "document_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "document_sequence"
    )
    private Long id;

    private String titre;

    private String author;

    private LocalDate date_publication;

    public Document(String titre, String author, LocalDate datePublication) {
        this.titre = titre;
        this.author = author;
        this.date_publication = datePublication;
    }

}
