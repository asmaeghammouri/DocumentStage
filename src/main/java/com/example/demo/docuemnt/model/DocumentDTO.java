package com.example.demo.docuemnt.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
public class DocumentDTO {

    @NotBlank(message = "titre is required")
    private String titre;

    private String author;

    @Past(message = "future date")
    private LocalDate date_publication;


}
