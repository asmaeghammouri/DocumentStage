package com.example.demo.docuemnt.service;


import com.example.demo.docuemnt.exception.AucunDocumentTrouveException;
import com.example.demo.docuemnt.exception.DocumentInexistantException;
import com.example.demo.docuemnt.exception.ListeDocumentVideException;
import com.example.demo.docuemnt.mapper.DocumentMapper;
import com.example.demo.docuemnt.model.Document;
import com.example.demo.docuemnt.model.DocumentDTO;
import com.example.demo.docuemnt.repository.QDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentService {

    @Autowired
    QDocumentRepository documentRepository;

    @Autowired
    DocumentMapper documentMapper;

    public List<Document> getDocuments() {

        if(documentRepository.getAll().isEmpty()){
            throw new ListeDocumentVideException("la liste des documents est vide");
        }
        else{
            return documentRepository.getAll();
        }
    }

    public Optional<Document> getDocumentById(Long id) {
        if(!documentRepository.findById(id).isPresent()){
            throw new DocumentInexistantException("ce id n'existe pas");
        }
        return documentRepository.findById(id);
    }

    public List<Document> getDocumentParMotCle(String motCle) {
      if(documentRepository.getDocumentsParMotCle(motCle).isEmpty()){
          throw new AucunDocumentTrouveException("n'existe aucun document contient ce mot ");
        }
      return documentRepository.getDocumentsParMotCle(motCle);
    }

    public DocumentDTO transfert(Long document){
        Optional<Document> document1 = documentRepository.findById(document);
        if(!document1.isPresent()){
            throw new DocumentInexistantException("ce id n'existe pas ");
        }
        return documentMapper.asDocumentDTO(document1.get());
    }

    public Document transfert2(DocumentDTO documentDTO){
        return documentMapper.asDocument(documentDTO);
    }


}
