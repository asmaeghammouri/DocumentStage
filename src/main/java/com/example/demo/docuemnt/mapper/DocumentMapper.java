package com.example.demo.docuemnt.mapper;

import com.example.demo.docuemnt.model.Document;
import com.example.demo.docuemnt.model.DocumentDTO;
import fr.xebia.extras.selma.Mapper;


import static fr.xebia.extras.selma.IoC.SPRING;

//cette interface pour passer Document to documentDto

@Mapper(withIgnoreFields = {"id"}, withIoC =SPRING)
public interface DocumentMapper {

    DocumentDTO asDocumentDTO(Document document);
    Document asDocument(DocumentDTO documentDTO);

}
