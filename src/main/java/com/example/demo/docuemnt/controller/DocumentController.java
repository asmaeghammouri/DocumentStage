package com.example.demo.docuemnt.controller;

import com.example.demo.docuemnt.model.DocumentDTO;
import com.example.demo.docuemnt.repository.QDocumentRepository;
import com.example.demo.docuemnt.service.DocumentService;
import com.example.demo.docuemnt.model.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @Autowired
    QDocumentRepository qDocumentRepository;

    @GetMapping("/api/document")
    public ResponseEntity<List<Document>> getDocuments() {
            return new ResponseEntity<>(documentService.getDocuments(), HttpStatus.ACCEPTED);
    }

    @GetMapping("/api/document/{id}")
    public ResponseEntity<?> getDocumentById(@PathVariable Long id)  {
            return new ResponseEntity<>(documentService.getDocumentById(id), HttpStatus.ACCEPTED);
    }

    @GetMapping("/api/document/mot/{motCle}")
    public ResponseEntity<List<Document>> getDocumentParMotCle(@PathVariable String motCle){
       return new ResponseEntity<>(documentService.getDocumentParMotCle(motCle), HttpStatus.ACCEPTED);
    }

    @GetMapping("/dto/{id}")
    public ResponseEntity<DocumentDTO> getDocumentDTO(@PathVariable Long id){
        return new ResponseEntity<>(documentService.transfert(id), HttpStatus.ACCEPTED);
    }

    @PostMapping("/document")
    public ResponseEntity<Document> getDocument(@Valid @RequestBody DocumentDTO documentDTO){
        return new ResponseEntity<>(documentService.transfert2(documentDTO), HttpStatus.ACCEPTED);
    }

    @PostMapping("/addDocument")
    public ResponseEntity<Document> addDocument(@Valid @RequestBody Document document) throws MethodArgumentNotValidException {
        return new ResponseEntity<>(qDocumentRepository.save(document), HttpStatus.ACCEPTED);
    }

}
