package com.example.demo.docuemnt.querydsl;


import com.example.demo.docuemnt.model.QDocument;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;

@DataJpaTest
public class QdocumentTestsQueryDsl {

    @Autowired
    private EntityManager entityManager;

    void functionTestAllDocuments(){
        QDocument qDocument = QDocument.document;
        JPAQuery<QDocument> query = new JPAQuery<>(entityManager);
        query.from(qDocument).where(qDocument.author.eq("zahia"));
    }
}
