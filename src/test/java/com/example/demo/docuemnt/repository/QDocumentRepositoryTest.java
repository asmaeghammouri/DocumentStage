package com.example.demo.docuemnt.repository;

import com.example.demo.docuemnt.model.Document;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class QDocumentRepositoryTest {

    @Autowired
    QDocumentRepository qDocumentRepository;

    @Test
    void getAll() {
    }

    @Test
    void getDocumentsParMotCle() {
        List<Document> documentExcepted = new ArrayList<Document>();

        Document document1 = new Document();


        document1.setTitre("kkk");
        document1.setAuthor("abcdefg");
        document1.setDate_publication(null);


        String titre = "k";

        Document doc1 = qDocumentRepository.save(document1);


        documentExcepted.add(doc1);

        List<Document> test = qDocumentRepository.getDocumentsParMotCle(titre);
        int i=0;
        for (Document document2: test) {
                assertEquals(document2.getId(), documentExcepted.get(i).getId());
                i++;
    }
    }
    @Test
    void findById() {
        Document documentExpected =  new Document();

        documentExpected.setAuthor("athor1");
        documentExpected.setTitre("titre1");
        documentExpected.setDate_publication(null);
        Document document = qDocumentRepository.save(documentExpected);

        Document test = qDocumentRepository.findById(document.getId()).get();

        assertEquals(test.getId(), document.getId());
        assertEquals(9L, document.getId());

    }
}